<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
	<div class="header-top sticky">
		<div class="container position-relative">
			<div class="row align-items-center justify-content-between">
				<div class="col">
					<div class="submenu-wrapper" id="submenu-wrapper"></div>
					<nav id="MainNav" class="h-100">
						<div id="MobNavBtn">
							<span></span>
							<span></span>
							<span></span>
						</div>
						<?php getMenu('header-menu', '3', '', 'main_menu h-100'); ?>
					</nav>
				</div>
				<?php if ($logo = opt('logo')) : ?>
					<div class="col-lg-2 col-md-3 col-sm-6 col-9 logo-col">
						<a href="/" class="logo">
							<img src="<?= $logo['url'] ?>" alt="logo">
						</a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php if ($messages = opt('news_item')) : ?>
		<div class="header-bottom">
			<div class="top-events main-ticker-wrap">
				<div class="pause-news">
					<img src="<?= ICONS ?>pause.png" alt="pause">
				</div>
				<div class="msg-ticker-wrap" style="animation-duration: 10s;">
					<?php foreach ($messages as $msg): ?>
						<div class="msg-item">
							<a href="<?= $msg['link']; ?>" class="event-link" style="direction: rtl;">
									<span class="base-large-text white-text font-weight-normal">
                                        <?= $msg['title'] ?>
                                    </span>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</header>
<div class="pop-trigger">
	<img src="<?= ICONS ?>pop-trigger.png" alt="contact-us">
</div>

<div class="pop-great">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-auto col-12">
				<div class="float-form">
					<span class="close-form">
						<img src="<?= ICONS ?>close.png" alt="close-form">
					</span>
					<div class="pop-form">
						<?php if ($popTitle = opt('pop_form_title')) : ?>
							<h2 class="pop-form-title">
								<?= $popTitle; ?>
							</h2>
						<?php endif; ?>
						<?php getForm('10'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
