<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$current_id = get_the_permalink();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
		'posts_per_page' => 4,
		'post_type' => 'post',
		'post__not_in' => array($postId),
		'tax_query' => [
				[
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $post_terms,
				],
		],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 4,
			'orderby' => 'rand',
			'post_type' => 'post',
			'post__not_in' => array($postId),
	]);
}
get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
<article class="article-page-body">
	<div class="container pt-3">
		<div class="row justify-content-between align-items-stretch">
			<div class="col-xl-9 col-lg-8 col-12 d-flex flex-column align-items-start">
				<div class="base-output page-output">
					<h1><?php the_title(); ?></h1>
					<p class="post-page-date">
						<?php echo 'תאריך '.get_the_date('d/m/Y'); ?>
					</p>
					<h6><?php echo get_the_author_meta('display_name'); ?></h6>
					<?php the_content(); ?>
				</div>
				<?php if ($fields['post_file']) : ?>
					<a class="link-post-save" download href="<?= $fields['post_file']['url']; ?>">
						<span class="file-icon-wrap">
							<img src="<?= ICONS ?>file.png">
						</span>
						<span class="file-save-text">
							<?= (isset($fields['post_file']['title']) && $fields['post_file']['title']) ?
								$fields['post_file']['title'] : ''; ?>
						</span>
					</a>
				<?php endif; ?>
				<div class="social-trigger">
						<span class="social-item">
							<img src="<?= ICONS ?>share.png" alt="share">
						</span>
					<div class="all-socials item-socials" id="show-socials">
						<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $current_id; ?>" target="_blank"
						   class="socials-wrap">
							<img src="<?= ICONS ?>share-whatsapp.png" alt="share-whatsapp">
						</a>
						<a class="socials-wrap" href="https://www.facebook.com/sharer/sharer.php?u=#<?= $current_id; ?>"
						   target="_blank">
							<img src="<?= ICONS ?>share-facebook.png" alt="share-facebook">
						</a>
						<a class="socials-wrap" href="mailto:?&subject=&body=<?= $current_id; ?>">
							<img src="<?= ICONS ?>share-mail.png" alt="share-mail">
						</a>
					</div>
					<span class="file-save-text mr-1">
							שתפו את המאמר עם חברים
						</span>
				</div>
				<div class="comments-block">
					<?php comments_template(); ?>
				</div>
				<?php if ($fields['faq_item']) :
					get_template_part('views/partials/content', 'faq',
							[
									'title' => $fields['faq_title'],
									'faq' => $fields['faq_item'],
							]);
				endif; ?>
			</div>
			<?php if ($samePosts) : ?>
				<div class="col-xl-3 col-lg-4 col-12 mt-lg-0 mt-4">
					<div class="sticky-sidebar">
						<div class="row posts-output align-items-stretch">
							<div class="col-12">
								<h4 class="block-subtitle">
									<?= $fields['same_title'] ? $fields['same_title'] : 'מאמרים נוספים בנושא'; ?>
								</h4>
							</div>
							<?php foreach ($samePosts as $i => $post) {
								get_template_part('views/partials/card', 'post', [
										'post' => $post,
								]); } ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php $banner = $fields['banner_main'] ? $fields['banner_main'] : opt('banner_main');
if ($banner) : ?>
	<div class="container">
		<div class="row mt-3">
			<div class="col-12">
				<img src="<?= $banner['url']; ?>" alt="banner" class="w-100">
			</div>
		</div>
	</div>
<?php endif;
get_footer(); ?>
