<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$current_id = get_the_permalink();
$left_banner = $fields['pro_banner_left'] ? $fields['pro_banner_left'] : opt('pro_banner_left'); ?>
<article class="article-page-body">
	<div class="container pt-3 mb-5">
		<div class="row">
			<div class="col">
				<ul itemscope itemtype="https://schema.org/BreadcrumbList" class="breadcrumbs">
					<li itemprop="itemListElement" itemscope
						itemtype="https://schema.org/ListItem">
						<a itemprop="item" href="<?php echo get_home_url();?>">
							<span itemprop="name"><?php echo get_the_title( get_option('page_on_front') );?></span></a>
						<meta itemprop="position" content="1" />
					</li>
					<?= '&nbsp;'.'>'.'&nbsp;'; ?>
					<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
						<?php $id_countries = getPageByTemplate("views/categories.php"); ?>
						<a itemscope itemtype="https://schema.org/WebPage" itemprop="item" itemid="<?php echo get_permalink($id_countries); ?>"
						   href="<?php echo get_permalink($id_countries); ?>">
							<span itemprop="name"><?php echo get_the_title($id_countries); ?></span></a>
						<meta itemprop="position" content="2" />
					</li>
					<?= '&nbsp;'.'>'.'&nbsp;'; ?>
					<?php if (isset($fields['country_pro']) && $fields['country_pro']) : ?>
						<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem">
							<a itemprop="item" href="<?php echo get_term_link($fields['country_pro']); ?>">
								<span itemprop="name"><?php echo $fields['country_pro']->name; ?></span></a>
							<meta itemprop="position" content="3" />
						</li>
					<?php endif; ?>
					<?= '&nbsp;'.'>'.'&nbsp;'; ?>
					<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
						<a itemscope itemtype="https://schema.org/WebPage" itemprop="item" itemid="<?php echo get_permalink(); ?>"
						   href="<?php echo get_permalink(); ?>">
							<span itemprop="name"><?php echo get_the_title(); ?></span></a>
						<meta itemprop="position" content="4" />
					</li>
				</ul>
			</div>
		</div>
		<div class="row justify-content-between align-items-stretch">
			<div class="col-xl-10 col-lg-9 col-12 d-flex flex-column align-items-start">
				<div class="base-output page-output">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
				<?php if ($fields['project_gallery']) : ?>
					<div class="project-gallery-block">
						<div class="gallery-slider" dir="rtl">
							<?php foreach ($fields['project_gallery'] as $slide) : ?>
								<?php $img = ''; $video = '';
								if ($slide['acf_fc_layout'] === 'main_back_img') {
									$img = isset($slide['img']) ? $slide['img'] : '';
								} elseif ($slide['acf_fc_layout'] === 'main_back_video') {
									$video = isset($slide['video']) ? $slide['video'] : '';
								}
								if ($video) : ?>
									<div class="project-gallery-slide">
										<div class="gallery-slide-main slide-video" style="background-image: url('<?= getYoutubeThumb($video); ?>')">
											<span class="play-button play-button-design" data-video="<?= getYoutubeId($video); ?>">
												<img src="<?= ICONS ?>play-button.png" alt="play-video">
											</span>
										</div>
									</div>
								<?php else: ?>
									<div class="project-gallery-slide">
										<a class="gallery-slide-main" href="<?= $img ? $img['url'] : ''; ?>" data-lightbox="gallery-of-project"
										   style="background-image: url('<?= $img ? $img['url'] : ''; ?>')">
										</a>
									</div>
								<?php endif;
							endforeach; ?>
						</div>
					</div>
					<div class="video-modal">
						<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
							 aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
								<div class="modal-content">
									<div class="modal-body" id="iframe-wrapper"></div>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="close-icon">
								<img src="<?= ICONS ?>close.png">
							</span>
									</button>
								</div>
							</div>
						</div>
					</div>
					<div class="modal fade" id="reviewsModal" tabindex="-1" role="dialog"
						 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
							<div class="form-wrapper-pop">
					<span type="button" class="close-form" data-dismiss="modal" aria-label="Close">
						<img src="<?= ICONS ?>close.png">
					</span>
								<div class="modal-body" id="reviews-pop-wrapper"></div>
							</div>
						</div>
					</div>
				<?php endif;
				if (isset($fields['more_project_content']) && $fields['more_project_content']) : ?>
					<div class="base-output page-output">
						<?= $fields['more_project_content']; ?>
					</div>
				<?php endif;
				if ($fields['post_file']) : ?>
					<a class="link-post-save" download href="<?= $fields['post_file']['url']; ?>">
						<span class="file-icon-wrap">
							<img src="<?= ICONS ?>file.png">
						</span>
						<span class="file-save-text">
							<?= (isset($fields['post_file']['title']) && $fields['post_file']['title']) ?
								$fields['post_file']['title'] : ''; ?>
						</span>
					</a>
				<?php endif; ?>
				<div class="social-trigger">
						<span class="social-item">
							<img src="<?= ICONS ?>share.png" alt="share">
						</span>
					<div class="all-socials item-socials" id="show-socials">
						<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $current_id; ?>" target="_blank"
						   class="socials-wrap">
							<img src="<?= ICONS ?>share-whatsapp.png" alt="share-whatsapp">
						</a>
						<a class="socials-wrap" href="https://www.facebook.com/sharer/sharer.php?u=#<?= $current_id; ?>"
						   target="_blank">
							<img src="<?= ICONS ?>share-facebook.png" alt="share-facebook">
						</a>
						<a class="socials-wrap" href="mailto:?&subject=&body=<?= $current_id; ?>">
							<img src="<?= ICONS ?>share-mail.png" alt="share-mail">
						</a>
					</div>
					<span class="file-save-text mr-1">
							שתפו את המאמר עם חברים
						</span>
				</div>
			</div>
			<?php if ($left_banner) : ?>
				<div class="col-xl-2 col-lg-3 col-12">
					<div class="sticky-banner-left">
						<img src="<?= $left_banner['url']; ?>" alt="banner-side" class="w-100">
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php $samePosts = [];
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts == NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 4,
		'orderby' => 'rand',
		'post_type' => 'project',
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<div class="home-posts">
		<div class="container">
			<div class="row justify-content-start">
				<div class="col-auto">
					<h4 class="block-subtitle">
						<?= $fields['same_title'] ? $fields['same_title'] : 'כתבות על פי נושא חם'; ?>
					</h4>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($samePosts as $i => $post) {
					get_template_part('views/partials/card', 'post', [
						'post' => $post,
					]); } ?>
			</div>
		</div>
	</div>
<?php endif;
$banner = $fields['pro_banner_horizontal'] ? $fields['pro_banner_horizontal'] : opt('banner_main');
if ($banner) : ?>
	<div class="container">
		<div class="row mt-3">
			<div class="col-12">
				<img src="<?= $banner['url']; ?>" alt="banner" class="w-100">
			</div>
		</div>
	</div>
<?php endif;
get_footer(); ?>
