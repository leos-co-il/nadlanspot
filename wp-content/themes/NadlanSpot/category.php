<?php
get_header();
$query = get_queried_object();
$top = $query->parent === 0;
$show_projects = (isset($_GET['display'])) ? $_GET['display'] : NULL;
$number = 0;
if ($top && ($show_projects === NULL)) {
	$terms_children = get_terms([
			'taxonomy'      => 'category',
			'hide_empty'    => false,
			'parent'        => $query->term_id,
			'number'        => '9',
	]);
	$terms_children_all = get_term_children($query->term_id, 'category');
	$number = $terms_children_all ? count($terms_children_all) : 0;
} elseif ($top && $show_projects) {
	$posts = get_field('cat_projects', $query);
	if ($posts) {
		$count_posts = count($posts);
	}
	$page_id = $query->term_id;
} else {
	$posts = get_posts([
			'numberposts' => 6,
			'post_type' => 'post',
			'tax_query' => [
					[
							'taxonomy' => 'category',
							'field' => 'term_id',
							'terms' => $query->term_id,
					]
			]
	]);
	$published_posts = get_posts([
			'numberposts' => -1,
			'post_type' => 'post',
			'tax_query' => [
					[
							'taxonomy' => 'category',
							'field' => 'term_id',
							'terms' => $query->term_id,
					]
			]
	]);
	if ($published_posts) {
		$count_posts = count($published_posts);
	}
}
if ($top && ($show_projects === NULL)) : ?>
<section class="category-top-block">
	<?php if ($back_img = get_field('cat_back_img', $query)) : ?>
		<span class="background-category-top" style="background-image: url('<?= $back_img['url']; ?>')"></span>
	<?php endif; ?>
	<span class="middle-cat-overlay"></span>
	<div class="container">
		<div class="row">
			<div class="col-xl-5 col-md-7 col-12 content-category-page">
				<?php get_template_part('views/partials/repeat', 'breadcrumbs_line'); ?>
				<div class="category-top-line">
					<h2 class="category-top-title">
						<?= $query->name; ?>
					</h2>
					<?php if ($img = get_field('cat_img', $query)) : ?>
						<div class="flag-category-page">
							<img src="<?= $img['url']; ?>" alt="category-image">
						</div>
					<?php endif; ?>
				</div>
				<div class="base-output">
					<?= category_description(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<article class="category-output-block">
	<?php get_template_part('views/partials/repeat', 'topics');
	if ($terms_children) : ?>
		<div class="container my-3">
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php foreach ($terms_children as $term) {
					get_template_part('views/partials/card', 'theme',
							[
									'post' => $term,
							]);
				} ?>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php if ($number > 9) : ?>
	<div class="container">
		<div class="row justify-content-center mt-4">
			<div class="col-auto">
				<div class="more-link load-more-posts" data-type="category" data-count="<?= $number; ?>"
					 data-parent="<?= $query->term_id; ?>">
					טען עוד נושאים
					<img src="<?= ICONS ?>arrow-load-more.png" alt="load-more">
				</div>
			</div>
		</div>
	</div>
<?php endif;
else :
$side_banner_1 = get_field('subcat_banner_side_1', $query) ? get_field('subcat_banner_side_1', $query) : opt('subcat_banner_side_1');
$side_banner_2 = get_field('subcat_banner_side_2', $query) ? get_field('subcat_banner_side_2', $query) : opt('subcat_banner_side_2');
	?>
	<article class="category-sub-block mt-3">
		<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
		<div class="container pt-2">
			<div class="row">
				<div class="<?= $side_banner_1 ? 'col-lg-10' : 'col-12'; ?> col-main-content">
					<div class="row">
						<div class="col-12">
							<h1 class="subcategory-page-title"><?= $query->name; ?></h1>
							<div class="<?= ($show_projects === NULL) ? 'subcategory-description' : 'base-output'; ?>"><?= category_description(); ?></div>
						</div>
					</div>
					<?php if ($posts) : ?>
						<div class="row justify-content-center align-items-stretch put-here-posts">
							<?php foreach ($posts as $y => $post) {
								if ($y < 6 && $show_projects) {
									get_template_part('views/partials/card', 'post',
											[
													'post' => $post,
													'num' => $y,
											]);
								} elseif (!$show_projects) {
									get_template_part('views/partials/card', 'post',
											[
													'post' => $post,
											]);
								}
							} ?>
						</div>
					<?php endif; ?>
				</div>
				<?php if ($side_banner_1) : ?>
					<div class="col-lg-2 col-12">
						<div class="sticky-banner-left">
							<img src="<?= $side_banner_1['url']; ?>" alt="banner-side" class="w-100">
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</article>
<?php if ($posts && isset($count_posts) && $count_posts > 6) : ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div class="more-link load-more-posts" data-type="<?= $show_projects ? 'project' : 'post' ?>" data-count="<?= $count_posts; ?>"
					 data-term="<?= $query->term_id; ?>" data-page="<?= $page_id; ?>">
					טען עוד
					<img src="<?= ICONS ?>arrow-load-more.png" alt="load-more">
				</div>
			</div>
		</div>
	</div>
<?php endif;
$horizontal_banner = get_field('subcat_banner', $query) ? get_field('subcat_banner', $query) : opt('subcat_banner');
if ($horizontal_banner) : ?>
	<div class="container">
		<div class="row mb-3">
			<div class="col-12">
				<img src="<?= $horizontal_banner['url']; ?>" alt="banner" class="w-100">
			</div>
		</div>
	</div>
<?php endif;
endif;
if ($show_projects || !$top) :
	if ($show_projects) {
		$related_terms = get_terms([
				'taxonomy'      => 'category',
				'hide_empty'    => false,
				'parent'        => $query->term_id,
				'number'        => '3',
		]);
		$parent_link = get_term_link($query);
		$parent_link_name = $query->name;
	} else {
		$related_terms = get_terms([
				'taxonomy'      => 'category',
				'hide_empty'    => false,
				'parent'        => $query->parent,
				'number'        => '3',
		]);
		$term_parent = get_term($query->parent);
		$parent_link = get_term_link($term_parent);
		$parent_link_name = $term_parent->name;
	}
 if ($related_terms) : ?>
	<section class="related-themes">
		<div class="container">
			<div class="row">
				<div class="col-auto">
					<h3 class="block-subtitle">נושאים דומים באותה מדינה</h3>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($related_terms as $term) {
					get_template_part('views/partials/card', 'theme',
							[
									'post' => $term,
							]);
				} ?>
			</div>
			<div class="row justify-content-end">
				<div class="col-auto">
					<a class="base-link" href="<?= $parent_link; ?>">
						<?= 'נושאים נוספים ב'.$parent_link_name; ?>
					</a>
				</div>
			</div>
		</div>
	</section>
<?php endif; endif;
$cats_more = get_field('more_cats', $query);
if (!$cats_more) {
	$cats_more = get_terms([
			'taxonomy'      => 'category',
			'hide_empty'    => false,
			'parent'        => 0,
			'exclude' => $query->term_id,
			'number' => 4
	]);
}
if ($cats_more) : ?>
	<section class="my-4">
		<?php
		get_template_part('views/partials/repeat', 'categories',
				[
						'posts' => $cats_more,
						'title' => get_field('more_cats_title', $query),
						'link' => get_field('more_cats_link', $query),
				]); ?>
	</section>
<?php endif;
$side_banner_2 = get_field('subcat_banner_side_2', $query) ? get_field('subcat_banner_side_2', $query) : opt('subcat_banner_side_2');
if ($show_projects || !$top) :
	if ($faq = get_field('faq_item', $query))?>
	<section class="faq-banner-block">
		<div class="container">
			<div class="row">
				<div class="<?= $side_banner_2 ? 'col-lg-10' : 'col-12'; ?> col-main-content">
					<?php get_template_part('views/partials/content', 'faq',
							[
									'title' => get_field('faq_title', $query),
									'faq' => $faq,
					]); ?>
				</div>
				<?php if ($side_banner_2) : ?>
					<div class="col-lg-2 col-12">
						<div class="sticky-banner-left">
							<img src="<?= $side_banner_2['url']; ?>" alt="banner-side" class="w-100">
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif;
$posts = get_field('hot_posts', $query) ? get_field('hot_posts', $query) : opt('hot_posts');
$posts_2 = get_field('hot_posts_countries', $query) ? get_field('hot_posts_countries', $query) : '';
if (($posts || $posts_2) && $top && !$show_projects) : ?>
	<section class="hot-block-back">
		<?php if ($posts_2) {
			get_template_part('views/partials/content', 'hot',
						[
							'posts' => $posts_2,
							'title' => get_field('hot_posts_title_countries', $query),
							'link' => get_field('hot_posts_link_countries', $query),
						]);
		}
		if ($posts) {
			get_template_part('views/partials/content', 'hot',
					[
							'posts' => $posts,
							'title' => get_field('hot_posts_title', $query) ? get_field('hot_posts_title', $query) : opt('hot_posts_title'),
							'link' => get_field('hot_posts_link', $query) ? get_field('hot_posts_link', $query) : opt('hot_posts_link')
					]);
		} ?>
	</section>
<?php endif;
get_footer(); ?>
