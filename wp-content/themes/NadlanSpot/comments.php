<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password,
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
$commenter = wp_get_current_commenter();
?>

<div id="comments" class="comments-area default-max-width <?php echo get_option( 'show_avatars' ) ? 'show-avatars' : ''; ?>">
	<?php
	add_filter('comment_form_fields', 'custom_reorder_comment_fields' );
	function custom_reorder_comment_fields( $fields ){
		$new_fields = array(); // сюда соберем поля в новом порядке

		$myorder = array('author', 'comment'); // нужный порядок

		foreach( $myorder as $key ){
			$new_fields[ $key ] = $fields[ $key ];
			unset( $fields[ $key ] );
		}

		return $new_fields;
	}
	comment_form(
		array(
			'fields' => [
				'author' => '<p class="comment-form-author">
			<input id="author" name="author" placeholder="שם המגיב:" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . ' />
		</p>',
			'email' =>
				'<p class="comment-form-email"><label for="email">' . __( 'מייל', 'leos' ).
				'<span class="required"> (optional)</span></label>' .
				'<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ).
				'" size="30" aria-required="false" /></p>',
			],
			'comment_field'        => '<p class="comment-form-comment">
		<textarea id="comment" name="comment" cols="45" rows="8"  aria-required="true" required="required" placeholder="תגובה :"></textarea>
	</p>',
			'comment_notes_before' => '',
			'comment_notes_after'  => '',
			'label_submit' => esc_html__( 'שלח הודעה', 'leos' ),
		)
	);
	?>
	<?php
	if ( have_comments() ) :
		?>
		<ol class="comment-list">
			<?php
			wp_list_comments(
				array(
					'style'       => 'ol',
					'callback'      => 'custom_comments',
					'short_ping'  => true,
				)
			);
			?>
		</ol><!-- .comment-list -->
		<?php if ( ! comments_open() ) : ?>
		<p class="no-comments"><?php esc_html_e( 'הערות סגורות.', 'leos' ); ?></p>
	<?php endif; ?>
	<?php endif; ?>
</div><!-- #comments -->
