<?php

add_action('wp_ajax_nopriv_ajax_function', 'ajax_function');
add_action('wp_ajax_more_ajax_function', 'ajax_function');

function ajax_function()
{

    if (!wp_verify_nonce($_REQUEST['nonce'], "my_user_vote_nonce")) {
        exit("No naughty business please");
    }

    $result['type'] = "error";
    $result['var'] = 1;

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
    die();
}
add_action('wp_ajax_nopriv_get_more_function', 'get_more_function');
add_action('wp_ajax_get_more_function', 'get_more_function');

function get_more_function() {
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$id_term = (isset($_REQUEST['termID'])) ? $_REQUEST['termID'] : '';
	$id_page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : '';
	$count = (isset($_REQUEST['countAll'])) ? $_REQUEST['countAll'] : '';
	$quantityNow = (isset($_REQUEST['quantity'])) ? $_REQUEST['quantity'] : '';
	$type = (isset($_REQUEST['postType'])) ? $_REQUEST['postType'] : 'post';
	$termName = (isset($_REQUEST['termName'])) ? $_REQUEST['termName'] : 'category';
	$parent = (isset($_REQUEST['parent'])) ? $_REQUEST['parent'] : 0;
	$ids = explode(',', $ids_string);
	$html = '';
	$result['html'] = '';
	if ($type === 'post') {
		$query = new WP_Query([
			'post_type' => $type,
			'posts_per_page' => 3,
			'post__not_in' => $ids,
			'suppress_filters' => false,
			'tax_query' => $id_term && $termName? [
				[
					'taxonomy' => $termName,
					'field' => 'term_id',
					'terms' => $id_term,
				]
			] : '',
		]); if ($query->have_posts()) {
			foreach ($query->posts as $item) {
				$html = load_template_part('views/partials/card', 'post', [
					'post' => $item,
				]);
				$result['html'] .= $html;
				if ($count <= ($quantityNow + 3)) {
					$result['quantity'] = true;
				}
			}
		}
	} elseif ($type === 'project') {
		$query_obj = get_term($id_page);
		$query = get_field('cat_projects', $query_obj);
		foreach ($query as $i => $item) {
			if (!in_array($i, $ids)) {
				$html = load_template_part('views/partials/card', 'post', [
					'post' => $item,
					'num' => $i,
				]);
				$result['html'] .= $html;
			}
		}
		if ($count <= ($quantityNow + 3)) {
			$result['quantity'] = true;
		}
	} elseif ($type === 'category' || $type === 'topic') {
		$num = $parent === 0 ? 2 : 3;
		$terms_all = get_terms([
			'taxonomy'      => 'category',
			'hide_empty'    => false,
			'parent'        => $parent,
			'exclude' => $ids,
			'number' => $num
		]);
		foreach ($terms_all as $item) {
			if ($parent !== 0) {
				$html = load_template_part('views/partials/card', 'theme', [
					'post' => $item,
				]);
			} else {
				$html = load_template_part('views/partials/card', 'country', [
					'country' => $item,
				]);
			}
			$result['html'] .= $html;
			if ($count <= ($quantityNow + $num)) {
				$result['quantity'] = true;
			}
		}
	} elseif ($type === 'worker') {
		$query = get_field('worker_item', $id_page);
		foreach ($query as $i => $item) {
			if (!in_array($i, $ids)) {
				$html = load_template_part('views/partials/card', 'worker', [
					'post' => $item,
					'num' => $i,
				]);
				$result['html'] .= $html;
			}
		}
		if ($count <= ($quantityNow + 4)) {
			$result['quantity'] = true;
		}
	}
	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}

