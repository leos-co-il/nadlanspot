<?php

add_action('admin_init', 'dev_settings');
function dev_settings() {
    add_settings_section(
        'dev_settings', // Section ID
        'הגדרות פיתוח', // Section Title
        'section_title', // Callback
        'general' // What Page?  This makes the section show up on the General Settings Page
    );

    add_settings_field( // Option 1
        'stage', // Option ID
        'Stage', // Label
        'select_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'dev_settings', // Name of our section
        array( // The $args
            'stage' // Should match Option ID
        )
    );

    register_setting('general','stage', 'esc_attr');

}

function section_title() {
    echo '<p>יש לבדוק את ההגדות לפני שליחת מייל ללקוח</p>';
}

function input_callback($args) {  // Textbox Callback
    $option = get_option($args[0]);
    echo '<input type="text" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" />';
}

function select_callback($args) {  // Select Callback
    $option = get_option($args[0]);

    $stage = [
        ['title' => 'Production', 'val'   => 'prod'],
        ['title' => 'Development', 'val'   => 'dev'],
        ['title' => 'Q/A', 'val'   => 'qa'],
    ];

    $output = '<select id="'. $args[0] .'" name="'. $args[0] .'">';

        foreach ($stage as $s){
            $is_current = ($s['val'] === $option) ? 'selected' : '';
            $output .=  '<option value="'.$s['val'].'" '.$is_current.' >'.$s['title'].'</option>';
        }

    $output .= '</select>';


    echo $output;
}


add_action('admin_notices', 'admin_msg');

function admin_msg(){
    if(ENV === 'dev'){
        echo '<div class="error dev-msg"><p>האתר נמצא במצב פיתוח</p><p><a href="/wp-admin/options-general.php">שינוי הגדרות</a></p></div>';
    }
}