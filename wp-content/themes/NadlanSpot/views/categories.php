<?php
/*
Template Name: מדינות
*/
the_post();
get_header();
$fields = get_fields();
$terms = get_terms([
	'taxonomy'      => 'category',
	'hide_empty'    => false,
	'parent'        => 0,
	'number'        => '12',
]);
$terms_all = get_terms([
		'taxonomy'      => 'category',
		'hide_empty'    => false,
		'parent'        => 0,
]);
$terms_all_count = $terms_all ? count($terms_all) : 0;
$count = $terms ? count($terms) : 0; ?>
<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row">
			<div class="col">
				<h1 class="page-title">
					<?php the_title(); ?>
				</h1>
				<div class="base-output">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($terms) : ?>
			<div class="row justify-content-center align-items-stretch <?= ($count < 9) ? 'put-here-posts' : ''; ?>">
				<?php foreach ($terms as $x => $term) {
					get_template_part('views/partials/card', 'country',
						[
							'country' => $term,
						]);
					if ($x === 7 && $fields['cats_banner']) : ?>
			</div>
			<div class="row my-3">
				<div class="col-12">
					<img src="<?= $fields['cats_banner']['url']; ?>" alt="banner" class="w-100">
				</div>
			</div>
		<div class="row justify-content-center align-items-stretch <?= ($terms_all_count > 12) ? 'put-here-posts' : ''; ?>">
		<?php endif; ?>
			<?php } ?>
		<?php endif;
		if ($terms_all_count > 12) : ?>
			<div class="row justify-content-center mt-4">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-type="category" data-count="<?= $terms_all_count; ?>"
						 data-parent="0">
						טען עוד מדינות
						<img src="<?= ICONS ?>arrow-load-more.png" alt="load-more">
					</div>
				</div>
			</div>
		<?php endif; ?>
		</div>
	</div>
</article>
<?php $posts = $fields['hot_posts'] ? $fields['hot_posts'] : opt('hot_posts');
if ($posts || $fields['hot_posts_countries']) : ?>
	<section class="hot-block-back">
		<?php if ($fields['hot_posts_countries']) {
			get_template_part('views/partials/content', 'hot',
						[
							'posts' => $fields['hot_posts_countries'],
							'title' => $fields['hot_posts_title_countries'] ? $fields['hot_posts_title_countries'] : '',
							'link' => $fields['hot_posts_link_countries'] ? $fields['hot_posts_link_countries'] : '',
						]);
		}
		if ($posts) {
			get_template_part('views/partials/content', 'hot',
					[
							'posts' => $posts,
							'title' => $fields['hot_posts_title'] ? $fields['hot_posts_title'] : opt('hot_posts_title'),
							'link' => $fields['hot_posts_link'] ? $fields['hot_posts_link'] : opt('hot_posts_link')
					]);
		} ?>
	</section>
<?php endif;
if ($fields['cats_banner_2']) : ?>
	<div class="container">
		<div class="row mt-3">
			<div class="col-12">
				<img src="<?= $fields['cats_banner_2']['url']; ?>" alt="banner" class="w-100">
			</div>
		</div>
	</div>
<?php endif;
get_footer(); ?>
