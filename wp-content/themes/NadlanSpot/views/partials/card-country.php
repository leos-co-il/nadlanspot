<?php if (isset($args['country']) && $args['country']) : $link = get_term_link($args['country']); ?>
	<div class="col-md-6 col-12 country-col">
		<div class="card-country more-card" data-id="<?= $args['country']->term_id; ?>">
			<div class="country-img">
				<?php if ($img = get_field('cat_img', $args['country'])) : ?>
					<img src="<?= $img['url']; ?>" alt="country-flag">
				<?php endif; ?>
			</div>
			<div class="country-card-content">
				<div class="card-content-inside">
					<h3 class="country-card-title"><?= $args['country']->name; ?></h3>
					<p class="post-card-text">
						<?= text_preview(category_description($args['country']), 15); ?>
					</p>
				</div>
				<div class="country-card-links">
					<a href="<?= $link; ?>" class="base-link">
						כל הנדלן במדינה
					</a>
					<?php if (get_field('cat_projects', $args['country'])) : ?>
						<a href="<?= $link; ?>?display=projects" class="base-link card-link-projects">
							כל הפרוייקטים במדינה
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
