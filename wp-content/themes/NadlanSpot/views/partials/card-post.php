<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-xl-3 col-md-6 col-12 mb-4 col-post">
		<div class="post-card more-card" data-id="<?= isset($args['num']) ? $args['num'] : $args['post']->ID; ?>">
			<a class="post-img" <?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')"
			<?php endif; ?> href="<?= $link; ?>">
				<div class="post-card-content">
					<h3 class="post-card-title"><?= $args['post']->post_title; ?></h3>
					<p class="post-card-date">
						<?php dateStyle($args['post']->post_date, 'd/m/Y'); ?>
					</p>
					<p class="post-card-text">
						<?= text_preview($args['post']->post_content, 10); ?>
					</p>
				</div>
			</a>
		</div>
	</div>
<?php endif; ?>
