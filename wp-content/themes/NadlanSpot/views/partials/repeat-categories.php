<?php if (isset($args['posts']) && $args['posts']) : ?>
	<div class="container mb-3">
		<?php if (isset($args['title']) && $args['title']) : ?>
			<div class="row justify-content-start">
				<div class="col-auto">
					<h3 class="block-subtitle">
						<?= $args['title']; ?>
					</h3>
				</div>
			</div>
		<?php endif; ?>
		<div class="row justify-content-center align-items-stretch">
			<?php foreach ($args['posts'] as $post) {
				get_template_part('views/partials/card', 'country',
					[
						'country' => $post,
					]);
			} ?>
		</div>
		<?php if (isset($args['link']) && $args['link']) : ?>
			<div class="row justify-content-end">
				<div class="col-auto">
					<a href="<?= $args['link']['url']; ?>" class="base-link">
						<?= (isset($args['link']['title']) && $args['link']['title']) ? $args['link']['title'] : 'מדינות נוספות';
						?>
					</a>
				</div>
			</div>
		<?php endif; ?>
	</div>
<?php endif; ?>
