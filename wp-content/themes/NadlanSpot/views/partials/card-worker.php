<?php if (isset($args['post']) && $args['post']) : ?>
	<div class="col-xl-3 col-lg-4 col-md-6 col-12 mb-4 col-post">
		<div class="post-card worker-card more-card" data-id="<?= isset($args['num']) ? $args['num'] : ''; ?>">
			<div class="post-img" <?php if ($img = $args['post']['img']) : ?>
				style="background-image: url('<?= $img['url']; ?>')" <?php endif; ?>>
				<div class="triggers-worker">
					<?php if (isset($args['post']['video']) && $args['post']['video']) : ?>
						<div class="trigger-worker video-trigger-worker play-button" data-video="<?= getYoutubeId($args['post']['video']); ?>">
							<img src="<?= ICONS ?>play.png" alt="watch-video">
						</div>
					<?php endif; ?>
					<?php if (isset($args['post']['text']) && $args['post']['text']) : ?>
						<div class="trigger-worker read-trigger-worker rev-pop-trigger">
							<img src="<?= ICONS ?>zoom.png" alt="read-more">
							<div class="take-worker-data">
								<div class="post-img" <?php if ($img = $args['post']['img']) : ?>
									style="background-image: url('<?= $img['url']; ?>')" <?php endif; ?>>
									<div class="post-card-content">
										<h3 class="post-card-title"><?= $args['post']['name']; ?></h3>
										<div class="base-output">
											<?= $args['post']['text']; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<div class="post-card-content">
					<h3 class="post-card-title"><?= $args['post']['name']; ?></h3>
					<p class="post-card-text">
						<?= $args['post']['desc']; ?>
					</p>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
