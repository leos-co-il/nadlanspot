<?php if (isset($args['post']) && $args['post']) : $link = get_term_link($args['post']); ?>
	<div class="col-lg-4 col-md-6 col-12 mb-4 col-post">
		<div class="post-card theme-card more-card" data-id="<?= $args['post']->term_id; ?>">
			<div class="post-img" <?php if ($img = get_field('cat_img', $args['post'])) : ?>
				style="background-image: url('<?= $img['url']; ?>')" <?php endif; ?>>
				<div class="post-card-content">
					<h3 class="post-card-title"><?= $args['post']->name; ?></h3>
					<p class="post-card-text">
						<?= text_preview(category_description($args['post']), 10); ?>
					</p>
					<a href="<?= $link; ?>" class="base-link">
						מאמרים בנושא
					</a>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
