<?php if ($topics = opt('topic_item')) : ?>
	<div class="container">
		<div class="row justify-content-center align-items-stretch">
			<?php foreach ($topics as $a => $topic) : ?>
				<div class="col-xl-4 col-md-6 col-12 topic-col">
					<div class="topic-item">
						<div class="topic-icon-wrap">
							<?php if ($topic['icon']) : ?>
								<img src="<?= $topic['icon']['url']; ?>" alt="topic-icon">
							<?php endif; ?>
						</div>
						<div class="topic-content">
							<h3 class="country-card-title mb-1"><?= $topic['title']; ?></h3>
							<p class="base-text"><?= $topic['desc']; ?></p>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>
