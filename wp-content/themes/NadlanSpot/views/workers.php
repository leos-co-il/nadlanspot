<?php
/*
Template Name: סוכנים
*/
the_post();
get_header();
$fields = get_fields();
$terms = $fields['worker_item'];
$count = $terms ? count($terms) : 0; ?>
<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row">
			<div class="col">
				<h1 class="page-title">
					<?php the_title(); ?>
				</h1>
				<div class="base-output">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($terms) : ?>
		<div class="row justify-content-center align-items-stretch <?= ($count > 12) ? 'put-here-posts' : ''; ?>">
			<?php foreach ($terms as $x => $term) {
				if ($x < 12) : get_template_part('views/partials/card', 'worker',
					[
						'post' => $term,
						'num' => $x
					]);
			endif; } ?>
		</div>
		<?php endif;
		if ($count > 12) : ?>
			<div class="row justify-content-center mt-4">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-type="worker" data-page="<?= get_queried_object_id(); ?>" data-count="<?= $count; ?>"
						 data-parent="0">
						טען עוד
						<img src="<?= ICONS ?>arrow-load-more.png" alt="load-more">
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<div class="video-modal">
	<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
		 aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body" id="iframe-wrapper"></div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="close-icon">
								<img src="<?= ICONS ?>close.png">
							</span>
				</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="reviewsModal" tabindex="-1" role="dialog"
	 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-body">
			<span type="button" class="close" data-dismiss="modal" aria-label="Close">
				<img src="<?= ICONS ?>close.png">
			</span>
			<div id="reviews-pop-wrapper" class="worker-data"></div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
