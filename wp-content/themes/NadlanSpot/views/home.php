<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<article class="home-main-block">
	<div class="container">
		<div class="row">
			<div class="<?= $fields['h_main_side_banner'] ? 'col-lg-10' : 'col-12'; ?> col-main-content">
				<?php if ($fields['h_main_post']) :
					$img = ($fields['h_main_post_img'] && isset($fields['h_main_post_img']['url'])) ? $fields['h_main_post_img']['url'] :
							(has_post_thumbnail($fields['h_main_post']) ? postThumb($fields['h_main_post']) : ''); ?>
					<div class="row flex-grow-1">
						<div class="<?= $img ? 'col-lg-8 col-md-6 col-12' : 'col-12'; ?>">
							<h2 class="home-block-title"><?= $fields['h_main_post']->post_title; ?></h2>
							<p class="post-page-date">
								<?php echo 'תאריך '; dateStyle($fields['h_main_post']->post_date, 'd/m/Y'); ?>
							</p>
							<h6><?php echo get_the_author_meta('display_name', $fields['h_main_post']->ID); ?></h6>
							<div class="base-output">
								<?= $fields['h_main_post_text'] ? $fields['h_main_post_text'] : text_preview($fields['h_main_post']->post_content, 40); ?>
							</div>
							<a href="<?= get_the_permalink($fields['h_main_post']); ?>" class="base-link">
								קרא עוד
							</a>
						</div>
						<?php if ($img) : ?>
							<div class="col-lg-4 col-md-6">
								<div class="post-img-inside" style="background-image: url('<?= $img; ?>')"></div>
							</div>
						<?php endif; ?>
					</div>
				<?php endif;
				if ($fields['h_main_posts']) : ?>
					<div class="row justify-content-center align-items-stretch mt-4">
						<?php foreach ($fields['h_main_posts'] as $post) {
							get_template_part('views/partials/card', 'post',
									[
											'post' => $post,
									]);
						} ?>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($fields['h_main_side_banner']) : ?>
				<div class="col-lg-2 col-12">
					<div class="sticky-banner-left">
						<img src="<?= $fields['h_main_side_banner']['url']; ?>" alt="banner-side" class="w-100">
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php if ($fields['h_countries']) : ?>
	<section class="home-countries">
		<div class="container">
			<?php if ($fields['h_countries_title']) : ?>
				<div class="row">
					<div class="col-auto">
						<h3 class="block-subtitle"><?= $fields['h_countries_title']; ?></h3>
					</div>
				</div>
			<?php endif; ?>
			<div class="row align-items-stretch">
				<?php foreach ($fields['h_countries'] as $h => $country) : ?>
					<div class="col-xl-3 col-lg-4 col-md-6 col-12 country-col">
						<a class="home-card-country" href="<?= get_term_link($country); ?>">
							<div class="country-img">
								<?php if ($img = get_field('cat_img', $country)) : ?>
									<img src="<?= $img['url']; ?>" alt="country-flag">
								<?php endif; ?>
							</div>
							<div class="country-card-content">
								<h3 class="country-card-title"><?= $country->name; ?></h3>
							</div>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
			<?php if (isset($fields['h_countries_link']['url'])) : ?>
				<div class="row justify-content-end">
					<div class="col-auto">
						<a href="<?= $fields['h_countries_link']['url']; ?>" class="base-link">
							<?= (isset($fields['h_countries_link']['title']) && $fields['h_countries_link']['title']) ? $fields['h_countries_link']['title'] : 'מדינות נוספות';
							?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['cats_banner']) : ?>
	<div class="container">
		<div class="row mb-4">
			<div class="col-12">
				<img src="<?= $fields['cats_banner']['url']; ?>" alt="banner" class="w-100">
			</div>
		</div>
	</div>
<?php endif;
if ($fields['home_posts_block'] || $fields['home_posts_main_post']) : ?>
	<section class="home-posts-output">
		<div class="container">
			<?php if ($fields['home_posts_all_title']) : ?>
				<div class="row">
					<div class="col-auto">
						<h3 class="block-subtitle"><?= $fields['home_posts_all_title']; ?></h3>
					</div>
				</div>
			<?php endif; ?>
			<div class="row">
				<?php if ($fields['home_posts_main_post']) : ?>
					<div class="<?= $fields['home_posts_block'] ? 'col-lg-6 col-12' : '';?>">
						<div class="row h-lg-100 h-auto align-items-lg-center align-items-stretch mb-lg-0 mb-3">
							<?php if (has_post_thumbnail($fields['home_posts_main_post'])) : ?>
								<div class="col-md-6 h-lg-100 h-auto mb-md-0 mb-3">
									<div class="post-img-inside" style="background-image:
											url('<?= postThumb($fields['home_posts_main_post']); ?>')"></div>
								</div>
							<?php endif; ?>
							<div class="<?= has_post_thumbnail($fields['home_posts_main_post']) ? 'col-md-6 col-12' : 'col-12'; ?>">
								<h2 class="home-block-title"><?= $fields['home_posts_main_post']->post_title; ?></h2>
								<p class="post-page-date">
									<?php echo 'תאריך '; dateStyle($fields['home_posts_main_post']->post_date, 'd/m/Y'); ?>
								</p>
								<h6><?php echo get_the_author_meta('display_name', $fields['home_posts_main_post']->ID); ?></h6>
								<div class="base-output">
									<p>
										<?=  text_preview($fields['home_posts_main_post']->post_content, 50); ?>
									</p>
								</div>
								<a href="<?= get_the_permalink($fields['home_posts_main_post']); ?>" class="base-link">
									קרא עוד
								</a>
							</div>
						</div>
					</div>
				<?php endif;
				if ($fields['home_posts_block']) : ?>
					<div class="<?= $fields['home_posts_main_post'] ? 'col-lg-6 col-12' : '';?>">
						<?php foreach ($fields['home_posts_block'] as $l => $post) : ?>
							<a class="card-post-home" href="<?= get_the_permalink($post); ?>">
								<span style="background-image: url('<?= has_post_thumbnail($post) ? postThumb($post) : ''; ?>')" class="card-post-home-img">
								</span>
								<div class="home-post-card-content">
									<div class="d-flex justify-content-start align-items-center flex-wrap">
										<h4 class="post-page-author"><?= $post->post_title; ?></h4>
										<p class="post-page-date"><?=  'תאריך '.get_the_date('d/m/Y', $post); ?></p>
									</div>
									<p class="base-text"><?= text_preview($post->post_content, 10); ?></p>
								</div>
							</a>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($fields['home_posts_all_link']) : ?>
				<div class="row justify-content-end mt-3">
					<div class="col-auto">
						<a href="<?= $fields['home_posts_all_link']['url']; ?>" class="base-link">
							<?= (isset($fields['home_posts_all_link']['title']) && $fields['home_posts_all_link']['title']) ? $fields['home_posts_all_link']['title'] : 'מאמרים נוספים בנושא';
							?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
$posts = $fields['hot_posts'] ? $fields['hot_posts'] : opt('hot_posts');
if ($posts || $fields['hot_posts_countries']) : ?>
	<section class="hot-block-back">
		<?php if ($fields['hot_posts_countries']) {
			get_template_part('views/partials/content', 'hot',
					[
							'posts' => $fields['hot_posts_countries'],
							'title' => $fields['hot_posts_title_countries'] ? $fields['hot_posts_title_countries'] : '',
							'link' => $fields['hot_posts_link_countries'] ? $fields['hot_posts_link_countries'] : '',
					]);
		}
		if ($posts) {
			get_template_part('views/partials/content', 'hot',
					[
							'posts' => $posts,
							'title' => $fields['hot_posts_title'] ? $fields['hot_posts_title'] : opt('hot_posts_title'),
							'link' => $fields['hot_posts_link'] ? $fields['hot_posts_link'] : opt('hot_posts_link')
					]);
		} ?>
	</section>
<?php endif;
if ($fields['cats_banner_2']) : ?>
	<div class="container">
		<div class="row mt-3">
			<div class="col-12">
				<img src="<?= $fields['cats_banner_2']['url']; ?>" alt="banner" class="w-100">
			</div>
		</div>
	</div>
<?php endif;
get_footer(); ?>
