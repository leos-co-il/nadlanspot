<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();
$tel = opt('tel');
$mail = opt('mail');
$whatsapp = opt('whatsapp');
$open_hours = opt('open_hours');
?>
<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="col-auto">
			<h1 class="page-title text-center"><?php the_title(); ?></h1>
		</div>
		<div class="col-12">
			<div class="base-output text-center">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
	<div class="contact-page-body">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xll-10 col-12">
					<div class="row justify-content-center align-items-center">
						<div class="col-xl-9 col-lg-7 col-12 col-contact-pad">
							<div class="row justify-content-center contacts-row">
								<?php if ($tel) : ?>
									<a href="tel:<?= $tel; ?>" class="col-sm-4 col-12 contact-item wow flipInX" data-wow-delay="0.2s">
										<div class="contact-icon-wrap">
											<img src="<?= ICONS ?>contact-tel.png" alt="phone">
										</div>
										<h3 class="contact-type-title">דברו איתנו</h3>
										<h4 class="contact-type"><?= $tel; ?></h4>
									</a>
								<?php endif;
								if ($mail) : ?>
									<a href="mailto:<?= $mail; ?>" class="col-sm-4 col-12 contact-item wow flipInX" data-wow-delay="0.4s">
										<div class="contact-icon-wrap">
											<img src="<?= ICONS ?>contact-mail.png" alt="email">
										</div>
										<h3 class="contact-type-title">כיתבו לנו</h3>
										<h4 class="contact-type"><?= $mail; ?></h4>
									</a>
								<?php endif;
								if ($open_hours) : ?>
									<div class="col-sm-4 col-12 contact-item wow flipInX" data-wow-delay="0.6s">
										<div class="contact-icon-wrap">
											<img src="<?= ICONS ?>contact-hours.png">
										</div>
										<h3 class="contact-type-title">השעות שלנו</h3>
										<h4 class="contact-type"><?= $open_hours; ?></h4>
									</div>
								<?php endif; ?>
							</div>
						</div>
						<div class="col-xl-3 col-lg-5 col-12">
							<div class="form-contact-wrap">
								<?php if ($fields['contact_form_title']) : ?>
									<h3 class="pop-form-title"><?= $fields['contact_form_title']; ?></h3>
								<?php endif;
								getForm('11'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
