<?php

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$open_hours = opt('open_hours');
$facebook = opt('facebook');
$youtube = opt('youtube');
$linkedin = opt('linkedin');
$menu_title_1 = opt('foo_menu_title_1');
$menu_title_2 = opt('foo_menu_title_2');
?>

<footer>
	<div class="footer-form">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-12">
					<?php if ($title = opt('foo_form_title')) : ?>
						<h2 class="foo-form-title"><?= $title; ?></h2>
					<?php endif;
					getForm('132'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-main">
		<a id="go-top">
			<img src="<?= ICONS ?>to-top.png" alt="to-top">
		</a>
		<div class="container footer-container-menu">
			<div class="row justify-content-center">
				<div class="col-lg col-6 foo-menu foo-main-menu">
					<h3 class="foo-title">סרגל ניווט</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '1'); ?>
					</div>
				</div>
				<div class="col-lg col-6 foo-menu foo-links-menu">
					<h3 class="foo-title">
						<?= $menu_title_1 ? $menu_title_1 : 'מאמרים אחרונים '; ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-links-1-menu', '1'); ?>
					</div>
				</div>
				<div class="col-lg col-6 foo-menu foo-links-menu">
					<h3 class="foo-title">
						<?= $menu_title_2 ? $menu_title_2 : 'מאמרים אחרונים '; ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-links-2-menu', '1'); ?>
					</div>
				</div>
				<div class="col-lg col-6 foo-menu foo-contacts-menu">
					<h3 class="foo-title">צור קשר</h3>
					<div class="menu-border-top">
						<ul class="contact-list">
							<?php  if ($address) : ?>
								<li>
									<a href="https://www.waze.com/ul?q=<?= $address; ?>" class="foo-link">
										<p class="foo-link-subtitle">
											<?= $address; ?>
										</p>
									</a>
								</li>
							<?php endif;
							if ($tel) : ?>
								<li>
									<a href="tel:<?= $tel; ?>" class="foo-link">
										<p class="foo-link-subtitle">
											<?= $tel; ?>
										</p>
									</a>
								</li>
							<?php endif;
							if ($mail) : ?>
								<li>
									<a href="mailto:<?= $mail; ?>" class="foo-link">
										<p class="foo-link-subtitle"><?= $mail; ?></p>
									</a>
								</li>
							<?php endif; ?>
						</ul>
						<div class="foo-socials-line">
							<?php if ($youtube) : ?>
								<a href="<?= $youtube; ?>" class="foo-social-link-item">
									<img src="<?= ICONS ?>youtube.png" alt="youtube">
								</a>
							<?php endif;
							if ($facebook) : ?>
								<a href="<?= $facebook; ?>" class="foo-social-link-item">
									<img src="<?= ICONS ?>facebook.png" alt="facebook">
								</a>
							<?php endif;
							if ($linkedin) : ?>
								<a href="<?= $linkedin; ?>" class="foo-social-link-item"
								   target="_blank">
									<img src="<?= ICONS ?>linkedin.png" alt="linkedin">
								</a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
